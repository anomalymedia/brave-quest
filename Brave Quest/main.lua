function love.load()
  noofretries = 0
  darttime = 0
  time = 0
  gametimer = 20
  gamestate = 0
  leverstate = 0
  levelnumber = 1
  lives = 3
  noofpups = 1
  dartlives = 1
  leveruses = 1
  deathSoundDart = love.audio.newSource("audio/SoundFx/arrow_x.wav","static")
  MenuMusic = love.audio.newSource("audio/Music/Escape.ogg")
  GameoverMusic = love.audio.newSource("audio/Music/Nevermore.mp3")
  InGameMusicL1 = love.audio.newSource("audio/Music/TreadLightly.ogg")
 InGameMusicL2 = love.audio.newSource("audio/Music/The Mysterious Ones.ogg")
  imgStartGameBtn = love.graphics.newImage("sprites/StartGameBtnOff.png")
	imgStartGameBtnOn =  love.graphics.newImage("sprites/StartGameBtnOn.png")
	imgExitGameBtn = love.graphics.newImage("sprites/ExitGameBtnOff.png")
	imgExitGameBtnOn =  love.graphics.newImage("sprites/ExitGameBtnOn.png")

	buttons = {
			{ imgOff = imgStartGameBtn, imgOn = imgStartGameBtnOn, x = 400, y = 300 - 86, w = 318, h = 86, action = "play"},
			{ imgOff = imgExitGameBtn, imgOn = imgExitGameBtnOn, x = 400, y = 400 - 86, w = 318, h = 86, action = "exit"},
    }
	player = {
  griddart_x = 680,
  griddart_y = 120,
  grid_x  = 80,
  grid_y  = 480,
  gridlever_x = 680,
  gridlever_y = 280,
  act_x = 40,
  act_y = 40,
  actdart_x = 40,
  actdart_y = 40,
  actlever_x = 680,
  actlever_y = 280,
  speed = 4,
  dartspeed = 7,
  character = love.graphics.newImage("sprites/person.png"),
  wall = love.graphics.newImage("sprites/FloorVar2.png"),
  back = love.graphics.newImage("sprites/stonewall.png"),
  ladder = love.graphics.newImage("sprites/laddertile.png"),
  bottomLadder = love.graphics.newImage("sprites/ladder.png"),
  spike = love.graphics.newImage("sprites/SpikeWall.png"),
  goal = love.graphics.newImage("sprites/player0.png"),
  gamewinscreen = love.graphics.newImage("sprites/WinScreen.png"),
  gamelosescreen = love.graphics.newImage("sprites/LoseScreen.png"),
  dart = love.graphics.newImage("sprites/DartTrap.png"),
  livepowerup = love.graphics.newImage("sprites/LifePowerup.png"),
  levera = love.graphics.newImage("sprites/activelever.png"),
  leveri = love.graphics.newImage("sprites/inactivelever.png")
  --SoundManager soundmanager
	}
	map = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 4, 1 },
		{ 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 2, 1},
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	}
  
  map2 = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 3, 0, 3, 0, 3, 0, 3, 5, 3, 0, 0, 0, 0, 4, 1 },
		{ 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 3, 0, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0, 3, 0, 0, 2, 1},
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	}
end
 
  function drawButton(off,on,x,y,w,h,mx,my) -- off sprite,on sprite,x position,y position,width,height,max x,max y
        local ins = insideBox(mx,my,x - (w/2),y - (h/2),w,h)
        
        love.graphics.setColor(255,255,255,255)
        
        if ins then
                love.graphics.draw(on,x,y,0,1,1,(w/2),(h/2))
        else
                love.graphics.draw(off,x,y,0,1,1,(w/2),(h/2))
        end
  end
 
function love.update(dt)
   if gamestate % 2 == 0 then
     if gamestate ~= 0 then
	player.actdart_x = player.actdart_x - ((player.actdart_x - player.griddart_x) * player.dartspeed * dt)
  
  	player.actdart_y = player.actdart_y - ((player.actdart_y - player.griddart_y) * player.dartspeed * dt)

  
player.actlever_x = player.actlever_x - ((player.actlever_x - player.gridlever_x) * dt)
  
player.actlever_y = player.actlever_y - ((player.actlever_y - player.gridlever_y) )
  
  darttime = darttime + 5
  if darttime >= 80 then
      player.griddart_x = player.griddart_x - 40
      darttime = 0
    end
    
    if player.griddart_x <= 40 then
      player.griddart_x = 680
      
    end
      
  if lives >= 1 then
	player.act_y = player.act_y - ((player.act_y - player.grid_y) * player.speed * dt)
	player.act_x = player.act_x - ((player.act_x - player.grid_x) * player.speed * dt)
  end
  if testFloor(0, 1) == true then
    time = time + 5
    if time >= 150 then
    player.grid_y = player.grid_y + 40
      time = 0
  end
  
  elseif testPowerUp(0, 1)  then
    time = time + 5
    if time >= 150 then
      player.grid_y = player.grid_y + 40
    if noofpups >= 1 then
      lives = lives + 1
    end
    noofpups = noofpups - 1
      time = 0
  end
   
  elseif testLadder(0,1) then
      time = 0
  end
  
  
   if testGoal(0, 1) == true then
    time = time + 5
    if time >= 150 then
    player.grid_y = player.grid_y + 40
      time = 0
       gamestate = gamestate + 2
          levelnumber = levelnumber + 1
            gametimer = 30
            player.grid_x  = 80
            player.grid_y  = 480
  end
  
   
  elseif testLadder(0,0) then
      time = 0
  end
  
  
     if testSpike(0, 1) == true then
    time = time + 5
      if time >= 150 then
        lives = lives - 1
        player.grid_x  = 80
        player.grid_y  = 480
        time = 0
    end
    
    elseif testLadder(0,0) then
      time = 0
  
end

 if testSpike(0, 0) == true then
    time = time + 5
      if time >= 150 then
        lives = lives - 1
        player.grid_x  = 80
        player.grid_y  = 480
        time = 0
    end
  end

if testLever(0,0) == true then
  if leveruses >= 1 then
    leverstate = leverstate + 1
    leveruses = leveruses - 1
  end
end

if testPlayer(0,0) == true then
  if dartlives >= 1 then
    lives = lives - 1
    love.audio.play(deathSoundDart)
    dartlives = dartlives - 1
    player.griddart_x = 680
    player.griddart_y = 120
    player.grid_x  = 80
    player.grid_y  = 480
  end
end

--while(gametimer > 0) do
if gametimer > 0 then
  if lives > 0 then
if gamestate % 2 == 0 then
   gametimer = tonumber(gametimer - dt)
  end
end
end

 if testGoal(0,0) == true then


end


end
end
end 

function love.draw()
  if gamestate == 0 then
  local x = love.mouse.getX()
  local y = love.mouse.getY()
  local w = 318
  local h = 86
 -- love.graphics.print(gamestate, 500, 20)
  love.audio.play(MenuMusic)
  love.graphics.setBackgroundColor( 190, 190, 190 )
  for k,v in pairs(buttons) do 
    drawButton(v.imgOff,v.imgOn,v.x,v.y,v.w,v.h,x,y)
end
  end
  
   if testGoal (0,0) == false then
     if gamestate == 2 then
       if leverstate == 0 then
  love.graphics.draw(player.leveri, player.actlever_x, player.actlever_y)
elseif leverstate == 1 then
    love.graphics.draw(player.levera, player.actlever_x, player.actlever_y)
end 
	love.graphics.draw(player.character, player.act_x, player.act_y)
if leverstate == 0 then
  love.graphics.draw(player.dart, player.actdart_x, player.actdart_y)
end
love.audio.stop(MenuMusic)
love.audio.play(InGameMusicL1)
	for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 1 then
				love.graphics.draw(player.wall, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 0 then
				love.graphics.draw(player.back, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 2 then
				love.graphics.draw(player.ladder, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 3 then
				love.graphics.draw(player.spike, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 4 then
				love.graphics.draw(player.goal, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 5 then
				love.graphics.draw(player.livepowerup, x * 40, y * 40)
			end
		end
	end
  
    if leverstate == 0 then
  love.graphics.draw(player.leveri, player.actlever_x, player.actlever_y)
elseif leverstate == 1 then
    love.graphics.draw(player.levera, player.actlever_x, player.actlever_y)
end 
    love.graphics.draw(player.character, player.act_x, player.act_y)
    if leverstate == 0 then
    love.graphics.draw(player.dart, player.actdart_x, player.actdart_y)
    end

  
  
love.graphics.setNewFont("fonts/MedusaGothic.ttf",14) 
love.graphics.print("time: " .. " " .. math.floor(gametimer), 60, 0)
--love.graphics.print(player.grid_x, 400, 0) -- testing code - shows player's x position
--love.graphics.print(player.grid_y, 420, 20) -- testing code - shows player's y position
--love.graphics.print(gamestate, 500, 20)
love.graphics.print("Level: " .. " " .. levelnumber, 150, 20)
love.graphics.print("Lives: " .. " " ..  lives, 300, 20)
love.graphics.print("Number of retries: " .. " " ..  noofretries, 540, 20)
--love.graphics.print(time, 0, 60) -- testing code - shows time before jumps or before jump lands
--love.graphics.print(noofpups, 0, 80) -- testing code - shows number of times powerup can be collected
--love.graphics.print(leveruses, 0, 160) -- testing code - shows number of times lever can be activated
--love.graphics.print(leverstate, 0, 180) shows lever's state - active or inactive


if lives == 0 then
  love.audio.stop(InGameMusicL1)
 -- love.audio.play(GameoverMusic)
  love.graphics.setBackgroundColor( 0, 0, 0 )
  love.graphics.draw(player.gamelosescreen)
end  

if gametimer <= 0 then
    love.audio.stop(InGameMusicL1)
   -- love.audio.play(GameoverMusic)
    love.graphics.setBackgroundColor( 0, 0, 0 )
    love.graphics.draw(player.gamelosescreen)
end


elseif testGoal(0,0) == true then
elseif gamestate == 4 then

  leverstate = 0
  leveruses = 1

if leverstate == 0 then
  love.graphics.draw(player.leveri, player.actlever_x, player.actlever_y)
elseif leverstate == 1 then
    love.graphics.draw(player.levera, player.actlever_x, player.actlever_y)
end 
	love.graphics.draw(player.character, player.act_x, player.act_y)
if leverstate == 0 then
  love.graphics.draw(player.dart, player.actdart_x, player.actdart_y)
end
love.audio.stop(InGameMusicL1)
love.audio.play(InGameMusicL2)

	for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 1 then
				love.graphics.draw(player.wall, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 0 then
				love.graphics.draw(player.back, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 2 then
				love.graphics.draw(player.ladder, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 3 then
				love.graphics.draw(player.spike, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 4 then
				love.graphics.draw(player.goal, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 5 then
				love.graphics.draw(player.livepowerup, x * 40, y * 40)
			end
		end
	end
  
    if leverstate == 0 then
  love.graphics.draw(player.leveri, player.actlever_x, player.actlever_y)
elseif leverstate == 1 then
    love.graphics.draw(player.levera, player.actlever_x, player.actlever_y)
end 
    love.graphics.draw(player.character, player.act_x, player.act_y)
    if leverstate == 0 then
    love.graphics.draw(player.dart, player.actdart_x, player.actdart_y)
    end

  
  
love.graphics.setNewFont("fonts/MedusaGothic.ttf",14) 
love.graphics.print("time: " .. " " .. math.floor(gametimer), 60, 0)
--love.graphics.print(player.grid_x, 400, 0) -- testing code - shows player's x position
--love.graphics.print(player.grid_y, 420, 20) -- testing code - shows player's y position
--love.graphics.print(gamestate, 500, 20)
love.graphics.print("Level: " .. " " .. levelnumber, 150, 20)
love.graphics.print("Lives: " .. " " ..  lives, 300, 20)
love.graphics.print("Number of retries: " .. " " ..  noofretries, 540, 20)
--love.graphics.print(time, 0, 60) -- testing code - shows time before jumps or before jump lands
--love.graphics.print(noofpups, 0, 80) -- testing code - shows number of times powerup can be collected
--love.graphics.print(leveruses, 0, 160) -- testing code - shows number of times lever can be activated
--love.graphics.print(leverstate, 0, 180) shows lever's state - active or inactive


if lives == 0 then
  love.graphics.setBackgroundColor( 0, 0, 0 )
  love.audio.stop(InGameMusicL2)
 -- love.audio.play(GameoverMusic)
  love.graphics.draw(player.gamelosescreen)
end  

if gametimer <= 0 then
    love.graphics.setBackgroundColor( 0, 0, 0 )
    love.audio.stop(InGameMusicL2)
    --love.audio.play(GameoverMusic)
    love.graphics.draw(player.gamelosescreen)
end

  elseif gamestate == 6 then
  love.graphics.setBackgroundColor( 0, 0, 0 )
  love.graphics.draw(player.gamewinscreen)
  end
  end
end
 
function love.keypressed(key)
  if lives >= 1 then
    if gametimer >= 0 then
	if key == "up" then
    if testLadder(0, 0) then
      if testMap(0, -1) then
			player.grid_y = player.grid_y - 40
      end
    elseif testMap(0, 1) == false  then
    player.grid_y = player.grid_y - 40
    end
	elseif key == "down" then
    if testLadder(0, 1) then
      if testMap(0, 1) then
        player.grid_y = player.grid_y + 40
      end
    end
	elseif key == "left" then
		if testMap(-1, 0) then
      player.grid_x = player.grid_x - 40
      if testGoal(0,0) then
          gamestate = gamestate + 2
          levelnumber = levelnumber + 1
            gametimer = 30
            player.grid_x  = 80
            player.grid_y  = 480
          end
      if  testSpike(0,0) then
        lives = lives - 1
  player.grid_x  = 80
  player.grid_y  = 480
    end  
    
     if testPowerUp(0, 0) then
        if noofpups >= 1 then
      lives = lives + 1
    end
    noofpups = noofpups - 1
  end
end

	elseif key == "right" then
		if testMap(1, 0) then
      player.grid_x = player.grid_x + 40
      if  testSpike(0,0) == true then
        lives = lives - 1
        player.grid_x  = 80
        player.grid_y  = 480
      end  
      
      if testGoal(0,0) then
          gamestate = gamestate + 2
          levelnumber = levelnumber + 1
            gametimer = 30
            player.grid_x  = 80
            player.grid_y  = 480
        end
        
      if testPowerUp(0, 0) then
        if noofpups >= 1 then
      lives = lives + 1
    end
    noofpups = noofpups - 1
  end

end
end
end
end
  if key == "r" then
 -- love.audio.stop(GameoverMusic)
  love.audio.stop(InGameMusicL1)
  love.audio.stop(InGameMusicL2)
  love.audio.play(InGameMusicL1)
  player.grid_x  = 80
  player.grid_y  = 480
  lives = 3
  gamestate = 2
  gametimer = 20
  noofretries = noofretries + 1
  noofpups = 1
  player.griddart_x = 680
  player.griddart_y = 120
  time = 0
  dartlives = 1
  leverstate = 0
  leveruses = 1
  end
end

function love.mousepressed(x,y,button)
      --  if button == "1" then 
           for k,v in pairs(buttons) do -- for every button in buttons check if  click is inside button
             local ins = insideBox( x, y, v.x - (v.w/2), v.y - (v.h/2), v.w, v.h )
             
             if ins then 
               if v.action == "play" then 
                  gamestate = 2 
                elseif v.action == "exit" then
                  love.event.quit()
                end
              end
            end
          end
       
        
  function insideBox(px,py,x,y,wx,wy)
    if px > x and px < x + wx then
      if py > y and py < y + wy then
        return true
      end
    end
    return false
  end

function testPlayer(x, y)
	if player.grid_x == player.griddart_x and
     player.grid_y == player.griddart_y then
		return true

	end
	return false
  end

function testLever(x, y)
	if player.grid_x == player.gridlever_x and
     player.grid_y == player.gridlever_y then
		return true

	end
	return false
  end

function testLadder(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 2 then
		return true
	end
	return false
end

function testSpike(x, y)
  if gamestate == 2 then
if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 3 then
		return true
    end

  elseif gamestate == 4 then
  if map2[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 3 then
		return true
  end
	return false
end
end


function testFloor(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 0 then
		return true
	end
	return false
end

function testGoal(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 4 then
		return true
	end
	return false
end

function testPowerUp(x, y)
  if gamestate == 2 then
if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 5 then
		return true
    end

  elseif gamestate == 4 then
  if map2[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 5 then
		return true
  end
	return false
end
end
 
function testMap(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 1 then
		return false
	end
  	if map2[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 1 then
		return false
	end
	return true
  
end



 
