function love.load()
  noofretries = 0
  darttime = 0
  time = 0
  gametimer = 10
  gamestate = 0
  levelnumber = 1
  lives = 3
	player = {
  griddart_x = 680,
  griddart_y = 440,
  grid_x = 640,
  grid_y = 160,
  act_x = 40,
  act_y = 40,
  actdart_x = 40,
  actdart_y = 40,
  speed = 10,
  dartspeed = 7,
  character = love.graphics.newImage("sprites/player4.png"),
  wall = love.graphics.newImage("sprites/stonewallcracked.png"),
  back = love.graphics.newImage("sprites/stonewall.png"),
  ladder = love.graphics.newImage("sprites/laddertile.png"),
  bottomLadder = love.graphics.newImage("sprites/ladder.png"),
  spike = love.graphics.newImage("sprites/Spikes.png"),
  goal = love.graphics.newImage("sprites/player0.png"),
  gamewinscreen = love.graphics.newImage("sprites/gamewin.png"),
  gamelosescreen = love.graphics.newImage("sprites/gameover.png"),
  dart = love.graphics.newImage("sprites/DartTrap.png")
  --SoundManager soundmanager
	}
	map = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1 },
		{ 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1},
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	}
  
  map2 = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1 },
		{ 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 2, 0, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1 },
		{ 1, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1},
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	}
end
 
function love.update(dt)
	player.actdart_x = player.actdart_x - ((player.actdart_x - player.griddart_x) * player.dartspeed * dt)
  darttime = darttime + 5
  if darttime >= 150 then
      player.griddart_x = player.griddart_x - 40
      darttime = 0
      end
      
  if lives >= 1 then
	player.act_y = player.act_y - ((player.act_y - player.grid_y) * player.speed * dt)
	player.act_x = player.act_x - ((player.act_x - player.grid_x) * player.speed * dt)
  end
  if testFloor(0, 1) == true then
    time = time + 5
    if time >= 150 then
    player.grid_y = player.grid_y + 40
      time = 0
  end

   
  elseif testLadder(0,0) then
      time = 0
  end
  
   if testGoal(0, 1) == true then
    time = time + 5
    if time >= 150 then
    player.grid_y = player.grid_y + 40
      time = 0
       gamestate = gamestate + 2
          levelnumber = levelnumber + 1
            gametimer = 50
            player.grid_x  = 80
            player.grid_y  = 480
  end
  
   
   
  elseif testLadder(0,0) then
      time = 0
  end
  
    if testSpike(0, 1) == true then
    time = time + 5
      if time >= 150 then
        player.grid_y = player.grid_y + 40
        lives = lives - 1
        player.grid_x  = 80
        player.grid_y  = 480
    end
  
   
   
  elseif testLadder(0,0) then
      time = 0
  end
--while(gametimer > 0) do
if gametimer > 0 then
  if lives > 0 then
if gamestate % 2 == 0 then
   gametimer = tonumber(gametimer - dt)
  end
end
end

 if testGoal(0,0) == true then
   if gamestate % 2 == 0 then
 gametimer = 50
end

end


end

function love.draw()
  
  
   if testGoal (0,0) == false then
     if gamestate == 0 then
	love.graphics.draw(player.character, player.act_x, player.act_y)
  love.graphics.draw(player.dart, player.actdart_x, player.actdart_y)
	for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 1 then
				love.graphics.draw(player.wall, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 0 then
				love.graphics.draw(player.back, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 2 then
				love.graphics.draw(player.ladder, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 3 then
				love.graphics.draw(player.spike, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map do
		for x=1, #map[y] do
			if map[y][x] == 4 then
				love.graphics.draw(player.goal, x * 40, y * 40)
			end
		end
	end
  

    love.graphics.draw(player.character, player.act_x, player.act_y)
    love.graphics.draw(player.dart, player.actdart_x, player.actdart_y)
  
  
love.graphics.setNewFont("fonts/MedusaGothic.ttf",14) 
love.graphics.print("time: " .. " " .. gametimer, 60, 0)
love.graphics.print(player.grid_x, 400, 0)
love.graphics.print(player.grid_y, 420, 20)
love.graphics.print(gamestate, 500, 20)
love.graphics.print("Level: " .. " " .. levelnumber, 150, 20)
love.graphics.print("Lives: " .. " " ..  lives, 300, 20)
love.graphics.print("Number of retries: " .. " " ..  noofretries, 540, 20)


if lives == 0 then
  love.graphics.draw(player.gamelosescreen)
end  

if gametimer <= 0 then
    love.graphics.draw(player.gamelosescreen)
end

elseif testGoal(0,0) == true then
elseif gamestate == 2 then
love.graphics.draw(player.character, player.act_x, player.act_y)
love.graphics.draw(player.dart, player.act_x, player.act_y)
	for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 1 then
				love.graphics.draw(player.wall, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 0 then
				love.graphics.draw(player.back, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 2 then
				love.graphics.draw(player.ladder, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 3 then
				love.graphics.draw(player.spike, x * 40, y * 40)
			end
		end
	end
  
  for y=1, #map2 do
		for x=1, #map2[y] do
			if map2[y][x] == 4 then
				love.graphics.draw(player.goal, x * 40, y * 40)
			end
		end
	end
  
     
  love.graphics.draw(player.character, player.act_x, player.act_y)
 -- love.graphics.draw(player.draw, player.actdart_x, player.actdart_y)
  
love.graphics.setNewFont("fonts/MedusaGothic.ttf",14) 
love.graphics.print("time: " .. " " .. gametimer, 60, 0)
love.graphics.print(player.grid_x, 400, 0)
love.graphics.print(player.grid_y, 420, 20)
love.graphics.print(gamestate, 500, 20)
love.graphics.print("Level: " .. " " .. levelnumber, 150, 20)
love.graphics.print("Lives: " .. " " ..  lives, 300, 20)

if lives == 0 then
  love.graphics.draw(player.gamelosescreen)
end
    elseif gamestate == 4 then
  love.graphics.draw(player.gamewinscreen)
  end
  end
end
 
function love.keypressed(key)
  if lives >= 1 then
    if gametimer >= 0 then
	if key == "up" then
    if testLadder(0, 0) then
      if testMap(0, -1) then
			player.grid_y = player.grid_y - 40
      end
    elseif testMap(0, 1) == false  then
    player.grid_y = player.grid_y - 40
    end
	elseif key == "down" then
    if testLadder(0, 1) then
      if testMap(0, 1) then
        player.grid_y = player.grid_y + 40
      end
    end
	elseif key == "left" then
		if testMap(-1, 0) then
      player.grid_x = player.grid_x - 40
      if testGoal(0,0) then
          gamestate = gamestate + 2
          levelnumber = levelnumber + 1
            gametimer = 50
            player.grid_x  = 80
            player.grid_y  = 480
          end
      if  testSpike(0,0) then
        lives = lives - 1
  player.grid_x  = 80
  player.grid_y  = 480
      end  
end

	elseif key == "right" then
		if testMap(1, 0) then
      player.grid_x = player.grid_x + 40
      if  testSpike(0,0) == true then
        lives = lives - 1
        player.grid_x  = 80
        player.grid_y  = 480
      end  
      if testGoal(0,0) then
          gamestate = gamestate + 2
          levelnumber = levelnumber + 1
            gametimer = 50
            player.grid_x  = 80
            player.grid_y  = 480
          end
end
end
end
end
  if key == "r" then
  player.grid_x  = 80
  player.grid_y  = 480
  lives = 3
  gamestate = 0
  gametimer = 50
  noofretries = noofretries + 1
  end
end

function testLadder(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 2 then
		return true
	end
	return false
end

function testSpike(x, y)
  if gamestate == 0 then
if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 3 then
		return true
    end

  elseif gamestate == 2 then
  if map2[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 3 then
		return true
  end
	return false
end
end


function testFloor(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 0 then
		return true
	end
	return false
end

function testGoal(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 4 then
		return true
	end
	return false
end
 
function testMap(x, y)
	if map[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 1 then
		return false
	end
  	if map2[(player.grid_y / 40) + y][(player.grid_x / 40) + x] == 1 then
		return false
	end
	return true
  
end

