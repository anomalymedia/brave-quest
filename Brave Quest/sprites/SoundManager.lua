--Sound Manager class


soundmanager = {}
soundmanager.queue = {}
soundmanager.playlist = {}
soundmanager.currentsong = -1

local function shuffle(a,b)
  return math.random(1,2) == 1
 end

function soundmanager.playSoundFx(sndData)
  local src = love.audio.newSource(sndData,"static")
  table.insert(self.queue,src)
  love.audio.play(src)
end

function soundmanager.playMusic(first,second,third,fourth,fifth)
  for i, v in ipairs(self.playlist) do
    love.audio.stop(v)
  end
  if type(first) == "table" then
    self.playlist = first
  else 
    self.playlist(first,second,third,fourth,fifth)
  end
  self.currentsong = 1
  love.audio.play(self.playlist[1])
end

function soundmanager.shuffle(first, ...)
  local playlist
  if type(first) == "table" then
    self.playlist = first
  else
    self.playlist = {first, ...}
  end
  table.sort(playlist,shuffle)
  return unpack(playlist)
end

function soundmanager:update(dt)
  local removelist = {}
  for i,v in ipairs(self.queue) do 
    if v:isStopped() then
      table.insert(removelist,i)
    end
  end
  for i, v in ipairs(removelist) do
      table.remove(self.queue, v-i+1)
   end
     if self.currentsong ~= -1 and self.playlist and self.playlist[self.currentsong]:isStopped() then
      self.currentsong = self.currentsong + 1
      if self.currentsong > #self.playlist then
         self.currentsong = 1
      end
      love.audio.play(self.playlist[self.currentsong])
   end
end
